import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5

Window {
    id: win_master_list
    width: 320
    height: 650
    visible: false
    color: "#F4E6D6"
    title: "Мастера"
    Page {
        Header{anchors.topMargin: 10}
        id: page
        anchors.fill: parent
        background: null
        Rectangle{
            id: bg_list
            color: "#DCAA6F"
            anchors.fill: parent
            anchors.centerIn: parent
            anchors.topMargin: 55
            width: parent.width
            height: 600
            radius: 25
            Rectangle{
                color: "#DCAA6F"
                anchors.fill: parent
                anchors.centerIn: parent
                anchors.topMargin: 20
                anchors.bottomMargin: 50
                width: parent.width
                Component {
                    id: my_delegate
                    List_masters {
                        width: parent.width * 0.95
                        height: 150
                    }
                }

                ListView {
                    id: my_list
                    clip: true
                    anchors.fill: parent
                    model: my_model
                    delegate: my_delegate
                    spacing: 20
                }
                ListModel {
                    id: my_model
                    ListElement {
                        imagePath: "qrc:/res/master1.jpg"
                        infomaster: "Name: Anatoly
Age: 69
Work experience: 35
Description: an experienced carpenter
with many years of experience. Anatoly
knows all the subtleties of working
with various types of wood and is
able to create unique products."
                    }
                    ListElement {
                        imagePath: "qrc:/res/master2.jpg"
                        infomaster: "Name: Nadezhda
Age: 32
Work experience: 6
Description: professional carpenter,
used to work at a furniture factory.
Nadezhda also participates in various
competitions and exhibitions, where
she presents her works."

                    }
                    ListElement {
                        imagePath: "qrc:/res/master3.jpg"
                        infomaster: "Name: Alexander
Age: 26
Work experience: 3
Description: an amateur carpenter
who is engaged in woodworking in
his spare time. He likes to experiment
and create unusual objects made of
wood."
                    }
                    ListElement {
                        imagePath: "qrc:/res/master1.jpg"
                        infomaster: "Name: Anatoly
Age: 69
Work experience: 35
Description: an experienced carpenter
with many years of experience. Anatoly
knows all the subtleties of working
with various types of wood and is
able to create unique products."
                    }
                }
            }
        }

        Footer {
            id: footer
            anchors.bottom: parent.bottom
            boximg: "res/box.png"
            homeimg: "res/home.png"
            porfolioimg: "res/porfolio.png"
            onHomeClicked: {
                    var mainPageComponent = Qt.createComponent("MainPage.qml");
                    if (mainPageComponent.status === Component.Ready) {
                        var mainPage = mainPageComponent.createObject(win_master_list);
                        mainPage.visible = true;
                        win_master_list.visible = false;

                    }
                }
            onBoxClicked: {
                // создаем и открываем страницу корзины
                var boxPageComponent = Qt.createComponent("BoxPage.qml");
                if (boxPageComponent.status === Component.Ready) {
                    var boxPage = boxPageComponent.createObject(win_master_list);
                    win_master_list.visible = false;
                    boxPage.visible = true;
                }
            }
            onProfileClicked: {
                // создаем и открываем страницу профиля
                var profilePageComponent = Qt.createComponent("ProfilePage.qml");
                if (profilePageComponent.status === Component.Ready) {
                    var profilePage = profilePageComponent.createObject(win_master_list);
                    win_master_list.visible = false;
                    profilePage.visible = true;
                }
            }
        }
    }
    Prof_Masters {id: prof_masters}
}


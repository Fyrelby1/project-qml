import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

Rectangle {
    width: parent.width
    height: 50
    color: "transparent"

    Row {
        spacing: 10

        // Бургер
        Back{
            id: bur
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.leftMargin: 10
            anchors.topMargin: 20
        }

        // Название сайта
        Image {
            id: name
            source: "qrc:/res/name.png"
            width: 100
            height: 30
            anchors.top: parent.top
            anchors.topMargin: 10
            anchors.left: bur.right
            anchors.leftMargin: 70
            horizontalAlignment: Qt.AlignHCenter
        }
    }
}

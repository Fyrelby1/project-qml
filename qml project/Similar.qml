import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5

Item {

   RowLayout{
       spacing: 5
       Rectangle{
           id: img
           width: 90
           height: 120
           Image {
               id: iimm
               width: 90
               height: 90
               source: similarImg
           }
           Rectangle{
               color: "white"
               width: 40
               height: 20
               radius: 9
               anchors.top: img.top
               anchors.right: img.right
               anchors.rightMargin: -10
               anchors.topMargin: 10

               Text {
                   font.bold: true
                   font.pixelSize: 12
                   color: "#454545"
                   anchors.centerIn: parent
                   text: costsim
               }
           }
           Text {
               anchors.top: iimm.bottom
               anchors.topMargin: 2
               font.bold: true
               text: nametoy
               color: "#454545"
           }
       }
   }
}

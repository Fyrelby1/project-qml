import QtQuick 2.15
import QtQuick.Controls 2.15

Rectangle {
    width: 25
    height: 25
    color: "transparent"

    Image {
        anchors.fill: parent
        source: "qrc:/res/rington.png" // Замените путь на свой путь к изображению бургера
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            main_page.hide()
            page_uvedom.show()
        }
    }
}

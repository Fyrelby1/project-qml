import QtQuick 2.15
import QtQuick.Controls 2.15

Window {
    id: win_favority
    visible: false
    width: 320
    height: 650
    title: "Страница Избранное"

    Rectangle{
            anchors.fill:parent
            gradient:Gradient{
            GradientStop{position:0;color:"#F4E6D6"}
            GradientStop{position:1;color:"#F4E6D6"}
            }
        }

    Headerformin {
        id: header
        anchors.top: parent.top
    }

    // Основное содержимое страницы
    TextingPage{
        text1: "Favourites"
    }

    Footer {
        id: footer
        anchors.bottom: parent.bottom
        boximg: "res/box.png"
        homeimg: "res/home.png"
        porfolioimg: "res/porfolio.png"
        onHomeClicked: {
                var mainPageComponent = Qt.createComponent("MainPage.qml");
                if (mainPageComponent.status === Component.Ready) {
                    var mainPage = mainPageComponent.createObject(win_favority);
                    mainPage.visible = true;
                    win_favority.visible = false;

                }
            }
        onBoxClicked: {
            // создаем и открываем страницу корзины
            var boxPageComponent = Qt.createComponent("BoxPage.qml");
            if (boxPageComponent.status === Component.Ready) {
                var boxPage = boxPageComponent.createObject(win_favority);
                win_favority.visible = false;
                boxPage.visible = true;
            }
        }
        onProfileClicked: {
            // создаем и открываем страницу профиля
            var profilePageComponent = Qt.createComponent("ProfilePage.qml");
            if (profilePageComponent.status === Component.Ready) {
                var profilePage = profilePageComponent.createObject(win_favority);
                win_favority.visible = false;
                profilePage.visible = true;
            }
        }
    }
}

import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.53

Window {
    id: win_burger
    visible: false
    width: 320
    height: 650
    title: "Главная страница сайта"

    Rectangle{
        anchors.fill:parent
        gradient:Gradient{
            GradientStop{position:0;color:"#000000"}
            GradientStop{position:1;color:"#000000"}
            }
        }

    HeaderforProfile {
        id: header
        anchors.top: parent.top
        brimg: "img/open_burg.png"
    }

    // Основное содержимое страницы
    Text{
        id: home
        anchors.top: header.bottom
        anchors.left: header.left
        anchors.leftMargin: 50
        text: "Главная"
        color: "#FFFFFF"
        font.pixelSize: 12
        MouseArea {
            anchors.fill: parent
            onClicked: {
                main_page.visible = true
                win_burger.visible = false
            }
        }
    }
    Text{
        id: katalog
        anchors.top: home.bottom
        anchors.left: home.left
        anchors.topMargin: 15
        text: "Каталог"
        color: "#FFFFFF"
        font.pixelSize: 12

        MouseArea {
            anchors.fill: parent
            onClicked: {
                // Обработка нажатия на бургер
                console.log("Нажата кнопка каталог!")
                // Добавьте свой код обработки бургера здесь
            }
        }
    }
    Text{
        id: profile
        anchors.top: katalog.bottom
        anchors.left: katalog.left
        anchors.topMargin: 15
        text: "Профиль"
        color: "#FFFFFF"
        font.pixelSize: 12

        MouseArea {
            anchors.fill: parent
            onClicked: {
                win_burger.hide()
                profile_page.show()
            }
        }
    }
    Text{
        id: about
        anchors.top: profile.bottom
        anchors.left: profile.left
        anchors.topMargin: 15
        text: "О нас"
        color: "#FFFFFF"
        font.pixelSize: 12

        MouseArea {
            anchors.fill: parent
            onClicked: {
                // Обработка нажатия на бургер
                console.log("Нажата кнопка о нас!")
                // Добавьте свой код обработки бургера здесь
            }
        }
    }
    ProfilePage{id: profile_page}
}

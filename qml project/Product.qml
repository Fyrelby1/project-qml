import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5

Window {
    id: page_product
    width: 320
    height: 650
    visible: false
    color: "#F4E6D6"
    title: qsTr("Товар")

    Rectangle{
        width: parent.width
        height: 450
        radius: 28
        anchors.top: parent.top
        anchors.topMargin: -30
        Rectangle{
            id:img
            width: parent.width
            height: 350
            Image {
                width: parent.width
                height: 320
                source: "res/toy2.jpg"
            }
            RowLayout{
                anchors.right: parent.right
                anchors.rightMargin: 10
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 10
                Rectangle{
                    width: 40
                    height: 40
                    color: "transparent"
                    Image {
                        width: 40
                        height: 40
                        source: "res/boxwhite.png"
                    }
                }
                Rectangle{
                    width: 40
                    height: 40
                    color: "transparent"
                    Image {
                        width: 40
                        height: 40
                        source: "res/favwhite.png"
                    }
                }
            }
        }
        ColumnLayout{
            anchors.top: img.bottom
            anchors.topMargin: -26
            anchors.horizontalCenter: parent.horizontalCenter
            Text {
                font.bold: true
                font.pixelSize: 20
                text: "Toy Car"
                color: "#454545"
            }
            Text {
                font.bold: true
                font.pixelSize: 25
                text: "$20"
                color: "#454545"
            }
            Text {
                text: "Master: Alexander
This set consists of wooden, homemade tools for your
children! The set includes a hand-sewn silk pouch."
                color: "#383838"
            }
        }
    }
    Rectangle{
        width: 120
        height: 60
        radius: 35
        anchors.bottom: lis_prod.top
        anchors.bottomMargin: -30
        anchors.left: parent.left
        anchors.leftMargin: 10
        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 5
            text: "Similar"
            color: "#515151"
        }
        MouseArea{
            anchors.fill: parent
            onClicked: {console.log("похожие")}
        }
    }
    Rectangle{
        width: 120
        height: 60
        radius: 35
        color: "#DCAA6F"
        anchors.bottom: lis_prod.top
        anchors.bottomMargin: -30
        anchors.left: parent.left
        anchors.leftMargin: 140
        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 5
            text: "Reviews"
            color: "white"
        }
        MouseArea{
            anchors.fill: parent
            onClicked: {
                page_product.hide()
                reviews.show()
            }
        }
        ReviewPage{id: reviews}
    }
    Rectangle{
        id: lis_prod
        color: "white"
        width: parent.width
        height: 170
        anchors.bottom: parent.bottom
        Component {
            id: my_delegate
            Similar {
                width: 90
                height: 170
            }
        }

        ListView {
            id: my_list
            orientation: ListView.Horizontal
            clip: true
            anchors.fill: parent
            model: my_model
            delegate: my_delegate
            spacing: 8
            anchors.topMargin: 8
            anchors.leftMargin: 8
        }
        ListModel {
            id: my_model
            ListElement {
                similarImg: "res/toy1.png"
                costsim: "$23"
                nametoy: "Toy Tools"
            }
            ListElement {
                similarImg: "res/toy1.png"
                costsim: "$23"
                nametoy: "Toy Tools"
            }
            ListElement {
                similarImg: "res/toy1.png"
                costsim: "$23"
                nametoy: "Toy Tools"
            }
            ListElement {
                similarImg: "res/toy1.png"
                costsim: "$23"
                nametoy: "Toy Tools"
            }
            ListElement {
                similarImg: "res/toy1.png"
                costsim: "$23"
                nametoy: "Toy Tools"
            }
            ListElement {
                similarImg: "res/toy1.png"
                costsim: "$23"
                nametoy: "Toy Tools"
            }
        }
    }



    Footer {
        id: footer
        anchors.bottom: parent.bottom
        boximg: "res/box.png"
        homeimg: "res/home.png"
        porfolioimg: "res/porfolio.png"
        onHomeClicked: {
                var mainPageComponent = Qt.createComponent("MainPage.qml");
                if (mainPageComponent.status === Component.Ready) {
                    var mainPage = mainPageComponent.createObject(page_product);
                    mainPage.visible = true;
                    page_product.visible = false;

                }
            }
        onBoxClicked: {
            // создаем и открываем страницу корзины
            var boxPageComponent = Qt.createComponent("BoxPage.qml");
            if (boxPageComponent.status === Component.Ready) {
                var boxPage = boxPageComponent.createObject(page_product);
                page_product.visible = false;
                boxPage.visible = true;
            }
        }
        onProfileClicked: {
            // создаем и открываем страницу профиля
            var profilePageComponent = Qt.createComponent("ProfilePage.qml");
            if (profilePageComponent.status === Component.Ready) {
                var profilePage = profilePageComponent.createObject(page_product);
                page_product.visible = false;
                profilePage.visible = true;
            }
        }
    }
}

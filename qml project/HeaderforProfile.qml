import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

Rectangle {
    width: parent.width
    height: 50
    color: "transparent"
    property var brimg

    Row {
        spacing: 10

        // Бургер
        BackBut{
            id: bur
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.leftMargin: 10
            anchors.topMargin: 20
            buttnimg: brimg
        }
    }
}

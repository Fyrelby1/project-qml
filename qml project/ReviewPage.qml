import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.53



Window {
    id: win_reviews
    visible: false
    width: 320
    height: 650
    title: "Отзывы сайта"
    signal similarClicked

    onSimilarClicked: {
        win_reviews.close()
        page_product.show()
    }

    Rectangle{
            anchors.fill:parent
            gradient:Gradient{
            GradientStop{position:0;color:"#F4E6D6"}
            GradientStop{position:1;color:"#F4E6D6"}
            }
        }

    Rectangle{
        //id: rev
        radius: 35
        height: 64
        width: parent.width / 3
        color: "#DCAA6F"
        anchors{
            top: parent.top
            topMargin: 25
            left: parent.left
            leftMargin: 20
        }
        Text{
            text: "Similar"
            color: "#FFFFFF"
            font.pixelSize: 14
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.leftMargin: 32
            anchors.topMargin: 3
        }
        MouseArea{
            anchors.fill: parent
            onClicked: { similarClicked()}
        }
    }

    Rectangle{
        radius: 35
        height: 64
        width: parent.width / 3
        color: "#FFFFFF"
        anchors{
            top: parent.top
            topMargin: 25
            left: parent.left
            leftMargin: 140
        }
        Text{
            text: "Reviews"
            color: "#515151"
            font.pixelSize: 14
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.leftMargin: 30
            anchors.topMargin: 3
        }
    }

    Rectangle{
        height: 550
        width: parent.width
        gradient:Gradient{
        GradientStop{position:0;color:"#FFFFFF"}
        GradientStop{position:1;color:"#FFFFFF"}
        }
        anchors{
            top: parent.top
            topMargin: 50
            centerIn: parent
        }

        ListView {
            anchors.top: parent.top
            anchors.topMargin: 15
            anchors.fill: parent
            anchors.centerIn: parent
            width: parent.width - 10
            clip: true
            spacing: 10 // Расстояние между элементами списка

            model: 20 // Количество элементов в списке, измените на желаемое значение

            delegate:
                Rectangle {
                    radius: 5
                    width: parent.width
                    height: 100
                    color: "#FFFFFF" // Цвет заливки прямоугольника
                    border.color: "#DCAA6F" // Цвет границы прямоугольника
                    border.width: 1
                    Row {
                        anchors.right: parent.right
                        anchors.top: parent.top
                        anchors.rightMargin: 5
                        anchors.topMargin: 5
                        spacing: 5 // Adjust the spacing between images as needed

                        Repeater {
                            model: 5
                            delegate: Image {
                                source: "res/Star.png" // Replace with your actual image source
                                width: 15
                                height: 15
                            }
                        }
                    }
                    Image {
                        id: ava
                        anchors.top: parent.top
                        anchors.topMargin: 5
                        anchors.left: parent.left
                        anchors.leftMargin: 10
                        source: "res/ava.png"
                        width: 30
                        height: 30
                        Text {
                            anchors.top: parent.top
                            anchors.left: parent.right
                            anchors.leftMargin: 5
                            id: name
                            text: qsTr("Валерий Милазде(official)")
                        }
                        Text {
                            anchors.bottom: parent.bottom
                            anchors.left: parent.right
                            anchors.leftMargin: 5
                            id: date
                            text: qsTr("7 октября")
                            font.pixelSize: 8
                            color: "#868686"
                        }
                    }

                    Text{
                        id:comm
                        anchors.top: ava.bottom
                        anchors.left: ava.left
                        anchors.topMargin: 5
                        text: "Прекрасные игрушки, всем советую"
                    }

                    Text{
                        anchors.top: comm.bottom
                        anchors.left: ava.left
                        anchors.topMargin: 25
                        text: "Пожаловаться"
                        color: "#D39B59"

                        Rectangle{
                            anchors.left: parent.right
                            anchors.leftMargin: 150

                            Image {
                                id: dislikeImage
                                source: "res/dislike.png"
                                property int clickCount: 0

                                MouseArea {
                                    anchors.fill: parent
                                    onClicked: {
                                        dislikeImage.clickCount += 1
                                        clickCountText.text = dislikeImage.clickCount.toString()
                                    }
                                }

                                Text {
                                    id: clickCountText
                                    anchors { bottom: parent.bottom; left: parent.right; bottomMargin: 2; leftMargin: 5}
                                    font.pixelSize: 16
                                    text: dislikeImage.clickCount.toString()
                                }
                            }

                        }

                        Rectangle{
                            anchors.left: parent.right
                            anchors.leftMargin: 190
                            anchors.top: parent.top
                            anchors.topMargin: -8

                            Image {
                                id: likeImage
                                source: "res/likes.png"
                                property int clickCount: 0

                                MouseArea {
                                    anchors.fill: parent
                                    onClicked: {
                                        likeImage.clickCount += 1
                                        clickCountText1.text = likeImage.clickCount.toString()
                                    }
                                }

                                Text {
                                    id: clickCountText1
                                    anchors { bottom: parent.bottom; left: parent.right; bottomMargin: -5; leftMargin: 5}
                                    font.pixelSize: 16
                                    text: likeImage.clickCount.toString()
                                }
                            }

                        }

                    }
                }
            }
        }
    Footer {
        id: footer
        anchors.bottom: parent.bottom
        boximg: "res/box.png"
        homeimg: "res/home.png"
        porfolioimg: "res/porfolio.png"
        onHomeClicked: {
                var mainPageComponent = Qt.createComponent("MainPage.qml");
                if (mainPageComponent.status === Component.Ready) {
                    var mainPage = mainPageComponent.createObject(win_reviews);
                    mainPage.visible = true;
                    win_reviews.visible = false;

                }
            }
        onBoxClicked: {
            // создаем и открываем страницу корзины
            var boxPageComponent = Qt.createComponent("BoxPage.qml");
            if (boxPageComponent.status === Component.Ready) {
                var boxPage = boxPageComponent.createObject(win_reviews);
                win_reviews.visible = false;
                boxPage.visible = true;
            }
        }
        onProfileClicked: {
            // создаем и открываем страницу профиля
            var profilePageComponent = Qt.createComponent("ProfilePage.qml");
            if (profilePageComponent.status === Component.Ready) {
                var profilePage = profilePageComponent.createObject(win_reviews);
                win_reviews.visible = false;
                profilePage.visible = true;
            }
        }
    }

}

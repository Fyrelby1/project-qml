import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Window 2.0

Window {
    id: win_forgot
    visible: false
    width: 320
    height: 650
    color: "#F4E6D6" // Цвет фона страницы
    signal backtomainforgotClicked
    onBacktomainforgotClicked: {
           win_forgot.close()
           regis_main.show()
       }
    Text {
        anchors.bottom: recov.top
        anchors.left: recov.left
        text: "Password Recovery"
        color: "#454545"
        font.pixelSize: 20
        font.bold: true
    }
    Image {
        id: logo
        anchors.topMargin: 15
        anchors.horizontalCenter: parent.horizontalCenter
        source: "qrc:/res/Kuromi.png"
        width: 70
        height: parent.height / 25
    }
    Recov {
        id: recov
        width: 300
        height: 600
        anchors.bottom: parent.bottom

        Rectangle {
            id:rect
            anchors.top: parent.top
            anchors.topMargin: 10
            anchors.left: parent.left
            anchors.leftMargin: 10
            anchors.right: parent.right
            anchors.rightMargin: 10
            radius: 9
            color: "#D39B59"
            anchors.horizontalCenter: parent.horizontalCenter
            height: 100

            Text {
                width: parent.width - 20 // Учитываем отступы слева и справа
                anchors.centerIn: parent
                text: "Specify the email address to which your profile is linked. The link is sent only to the linked email. Your username is written in the email with the link."
                color: "white" // Цвет текста (вы можете выбрать нужный цвет)
                wrapMode: Text.WordWrap // Режим переноса слов
                font.pixelSize: 12 // Размер шрифта
                font.bold: true
            }
        }


        Text {
            anchors.top: rect.bottom
            anchors.topMargin: 5
            anchors.left: rect.left
            text: "E-mail"
            color: "#454545"
            font.pixelSize: 14
            font.bold: true
        }
        TextField {
            id:txt
            anchors.topMargin: 25
            anchors.top: rect.bottom
            width:rect.width
            anchors.horizontalCenter: parent.horizontalCenter
            color: "#454545"

            font.pixelSize: 14
        }

        Rectangle {
            id: butt
            anchors.topMargin: 10
            anchors.top: txt.bottom
            width: txt.width
            height: txt.height + 5
            anchors.horizontalCenter: parent.horizontalCenter

            color: "#D39B59" // Цвет фона кнопки
            radius: 9 // Радиус закругления углов
            border.color: "#765329" // Цвет обводки
            border.width: 1

            Text {
                anchors.centerIn: parent
                text: "Send a link"
                color: "white"
            }

            MouseArea {
                anchors.fill: parent

                onClicked: {
                    console.log("Button clicked")
                    // Добавьте свой код обработки нажатия здесь
                }
            }
        }

        Text {
            id: log
            anchors.top: butt.bottom
            anchors.topMargin: 5
            anchors.left: rect.left
            // Пример значения отступа слева
            text: "How to log in via the social network?"
            color: "#454545"
            font.pixelSize: 13
            font.bold: true
        }

        Text {
            id:t1
            anchors.topMargin: 5
            anchors.top: log.bottom
            anchors.left: log.left
            // Пример значения отступа слева
            width: parent.width // Учитываем отступы слева и справа
            text: "If the account is registered through a specific social network (VK or Google), then you can log in only through it."
            color: "#454545"
            wrapMode: Text.WordWrap
            font.pixelSize: 12
        }
        Text {
            id:tex
            anchors.topMargin: 5
            anchors.top: t1.bottom
            anchors.left: log.left
            // Пример значения отступа слева
            width: butt.width // Учитываем отступы слева и справа
            text: "Select your social network (VK) below, if necessary, enter the password and login from the selected social network."
            color: "#454545"
            wrapMode: Text.WordWrap
            font.pixelSize: 12
        }
        Row {
            id:row
            anchors.top: tex.bottom
            anchors.topMargin: 5
            anchors.left: log.left
            spacing: 5 // Расстояние между иконками

            Image {
                source: "qrc:/res/vk.png" // Путь к первой иконке
                width: 30 // Ширина иконки (настройте по вашему усмотрению)
                height: 30 // Высота иконки (настройте по вашему усмотрению)
            }

            Image {
                source: "qrc:/res/gg.png" // Путь ко второй иконке
                width: 30 // Ширина иконки (настройте по вашему усмотрению)
                height: 30 // Высота иконки (настройте по вашему усмотрению)
            }
        }

        Rectangle {
            id: butt1
            anchors.topMargin: 10
            anchors.top: row.bottom
            width: txt.width
            height: txt.height + 5
            anchors.horizontalCenter: parent.horizontalCenter

            color: "#D39B59" // Цвет фона кнопки
            radius: 9 // Радиус закругления углов
            border.color: "#765329" // Цвет обводки
            border.width: 1

            Text {
                anchors.centerIn: parent
                text: "Go back to the main page"
                color: "white"
            }

            MouseArea {
                anchors.fill: parent
                onClicked: backtomainforgotClicked()
            }
        }



    }


    Footer_for_pass{
        id: foot
        anchors.bottom: parent.bottom
        width: parent.width // Присвойте ширину contentArea
    }
    MainPage{id: main_page}
}


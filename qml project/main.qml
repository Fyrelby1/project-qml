import QtQuick 2.15
import QtQuick.Controls 2.15

ApplicationWindow {
    id: main_win
    visible: true
    width: 320
    height: 650
    title: "Загрузочный экран"

    Rectangle {
        id: myRectangle
        width: parent.width
        height: parent.height

        Image {
            source: "qrc:/res/image1.png"
            anchors.fill: parent
            fillMode: Image.PreserveAspectCrop
        }

        Image {
            id: logoImage
            source: "qrc:/res/kuromi_white.png"
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            width: myRectangle.width / 1.5
            height: myRectangle.height / 3
            fillMode: Image.PreserveAspectFit
        }

        Image {
            id: click
            source: "qrc:/res/click.png"
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            width: myRectangle.width / 4
            height: myRectangle.height / 4
            fillMode: Image.PreserveAspectFit
        }

        Image {
            id: touch
            source: "qrc:/res/clickk.png"
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            width: myRectangle.width / 4
            height: myRectangle.height / 10
            fillMode: Image.PreserveAspectFit
        }
        MouseArea{
            anchors.fill: parent
            onClicked: {
                reg.show()
                main_win.hide()
            }
        }
    }

    Regis{id: reg}

}

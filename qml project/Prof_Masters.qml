import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5


Window {
    id: prof_master_win
    width: 320
    height: 650
    color: "#F4E6D6"
    visible: false
    title: qsTr("Профиль мастера")
    Page{
        Header{anchors.topMargin: 10}
        id: page
        anchors.fill: parent
        background: null
        Rectangle{
            id: recrW
            width: 300
            height: 520
            color: "white"
            radius: 30
            anchors.centerIn: parent
            anchors.topMargin: 55
            GridLayout{
                columns: 1
                rows: 3
                rowSpacing: 15
                anchors.centerIn: parent
                Rectangle{
                    width: 250
                    height: 250
                    Image {
                        anchors.centerIn: parent
                        width: 250
                        height: 250
                        source: "qrc:/res/master1.jpg"
                    }
                }
                Text {
                    text: "Name: Anatoly
Age: 69
Work experience: 35
Description: an experienced carpenter with many
years of experience. Anatoly knows all the
subtleties of working with various types of wood
and is able to create unique products."
                }
                Button {
                    anchors.topMargin: 25
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: 100
                    height: 25
                    text: "<font color=\"white\">Sign up</font>"
                    background: Rectangle {
                        width: 200
                        height: 25
                        anchors.centerIn: parent
                        color: "#D39B59"
                        radius: 9
                    }
                    onClicked: {
                        prof_master_win.close()
                        win_record.show()
                    }
                }

            }
        }

        Footer {
            id: footer
            anchors.bottom: parent.bottom
            boximg: "res/box.png"
            homeimg: "res/home.png"
            porfolioimg: "res/porfolio.png"
            onHomeClicked: {
                    var mainPageComponent = Qt.createComponent("MainPage.qml");
                    if (mainPageComponent.status === Component.Ready) {
                        var mainPage = mainPageComponent.createObject(prof_master_win);
                        mainPage.visible = true;
                        prof_master_win.visible = false;

                    }
                }
            onBoxClicked: {
                // создаем и открываем страницу корзины
                var boxPageComponent = Qt.createComponent("BoxPage.qml");
                if (boxPageComponent.status === Component.Ready) {
                    var boxPage = boxPageComponent.createObject(prof_master_win);
                    prof_master_win.visible = false;
                    boxPage.visible = true;
                }
            }
            onProfileClicked: {
                // создаем и открываем страницу профиля
                var profilePageComponent = Qt.createComponent("ProfilePage.qml");
                if (profilePageComponent.status === Component.Ready) {
                    var profilePage = profilePageComponent.createObject(prof_master_win);
                    prof_master_win.visible = false;
                    profilePage.visible = true;
                }
            }
        }
    }
}

import QtQuick 2.15
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5

Item {
    property int www: 130
    property url  fav: "res/favoritytoy.png"
    property url  boxt: "res/boxtoy.png"
    Rectangle{
        id:rect
        radius:20
        anchors.fill: parent
        anchors.topMargin: 20
        GridLayout{
            id: grid
            columns: 2
            rows: 2
            columnSpacing: 10

            Rectangle{
//                anchors.top: grid.top
                Layout.alignment: Qt.AlignTop
                width: 130
                height: 120
                ColumnLayout{
                    Rectangle{
                        id: img1
                        width: www
                        height: width
                        Image {
                            width: www
                            height: width
                            source: imagePath
                            MouseArea{
                                anchors.fill: parent
                                onClicked: {
                                    product.show()
                                    main_page.hide()
                                }
                            }
                            Product{id: product}
                        }
                        Rectangle{
                            color: "white"
                            width: 60
                            height: 30
                            radius: 9
                            anchors.top: img1.top
                            anchors.right: img1.right
                            anchors.rightMargin: -10
                            anchors.topMargin: 10

                            Text {
                                font.bold: true
                                font.pixelSize: 15
                                color: "#454545"
                                anchors.centerIn: parent
                                text: cost
                            }
                        }
                    }
                     RowLayout{
                        Text {
                            Layout.fillWidth: true
                            font.bold: true
                            color: "#454545"
                            text: name
                        }
                        Image {
                            source: boxt
                        }
                        Image {
                            source: fav
                        }
                    }
                }
            }
            Rectangle{
                id: rect2
                width: 130
                height: 160
                ColumnLayout{
                    Rectangle{
                        id: img2
                        width: www
                        height: www+50
                        Image {
                            width: www
                            height: www+50
                            source: imagePath2
                        }

                        Rectangle{
                            color: "white"
                            width: 60
                            height: 30
                            radius: 9
                            anchors.top: img2.top
                            anchors.right: img2.right
                            anchors.rightMargin: -10
                            anchors.topMargin: 10

                            Text {
                                font.bold: true
                                font.pixelSize: 15
                                color: "#454545"
                                anchors.centerIn: parent
                                text: cost2
                            }
                        }
                    }
                     RowLayout{
                        Text {
                            Layout.fillWidth: true
                            font.bold: true
                            color: "#454545"
                            text: name2
                        }
                        Image {
                            source: boxt
                        }
                        Image {
                            source: fav
                        }
                    }
                }
            }

            Rectangle{
                width: 130
                height: 160
                ColumnLayout{
                    Rectangle{
                        id: img3
                        width: www
                        height: www+50
                        Image {
                            width: www
                            height: www+50
                            source: imagePath3
                        }
                        Rectangle{
                            color: "white"
                            width: 60
                            height: 30
                            radius: 9
                            anchors.top: img3.top
                            anchors.right: img3.right
                            anchors.rightMargin: -10
                            anchors.topMargin: 10

                            Text {
                                font.bold: true
                                font.pixelSize: 15
                                color: "#454545"
                                anchors.centerIn: parent
                                text: cost3
                            }
                        }
                    }
                     RowLayout{
                        Text {
                            Layout.fillWidth: true
                            font.bold: true
                            color: "#454545"
                            text: name3
                        }
                        Image {
                            source: boxt
                        }
                        Image {
                            source: fav
                        }
                    }
                }
            }

            Rectangle{
                width: 130
                height: 160
                Layout.topMargin: 55
                ColumnLayout{
                    Rectangle{
                        id: img4
                        width: www
                        height: width
                        Image {
                            width: www
                            height: width
                            source: imagePath4
                        }
                        Rectangle{
                            color: "white"
                            width: 60
                            height: 30
                            radius: 9
                            anchors.top: img4.top
                            anchors.right: img4.right
                            anchors.rightMargin: -10
                            anchors.topMargin: 10

                            Text {
                                font.bold: true
                                font.pixelSize: 15
                                color: "#454545"
                                anchors.centerIn: parent
                                text: cost4
                            }
                        }
                    }
                     RowLayout{
                        Text {
                            Layout.fillWidth: true
                            font.bold: true
                            color: "#454545"
                            text: name4
                        }
                        Image {
                            source: boxt
                        }
                        Image {
                            source: fav
                        }
                    }
                }
            }
        }
    }

}

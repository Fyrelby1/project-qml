import QtQuick 2.15
import QtQuick.Controls 2.15

Rectangle {
    width: 30
    height: 17
    color: "transparent"
    property var buttnimg

    Image {
        anchors.fill: parent
        source: buttnimg // Замените путь на свой путь к изображению бургера
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            // Обработка нажатия на бургер
            console.log("Нажата кнопка назад!")
            // Добавьте свой код обработки бургера здесь
        }
    }
}

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Window 2.0

Window {
    visible: false
    width: 320
    height: 650
    color: "#F4E6D6"
    Header{
        anchors.top:parent.top
        anchors.topMargin: 15
        anchors.leftMargin: 50}

    Recov {
        id: recov
        width: 300
        height: 600
        anchors.top:parent.top
        anchors.topMargin: 65
        anchors.bottom: parent.bottom
        Text {
            id:txtt
            anchors.top: recov.top
            anchors.topMargin: 5
            anchors.left: rect.left
            text: "Specify the pick-up point"
            color: "#454545"
            font.pixelSize: 14
            font.bold: true
        }
        Rectangle {
            id:rect
            anchors.top: txtt.bottom
            anchors.topMargin: 10
            anchors.left: parent.left
            anchors.leftMargin: 10
            anchors.right: parent.right
            anchors.rightMargin: 10
            radius: 9
            color: "#D39B59"
            anchors.horizontalCenter: parent.horizontalCenter
            height: 40
            TextField {
                id:txt
                anchors.topMargin: 10
                anchors.top: rect.top
                anchors.leftMargin: 30
                anchors.rightMargin: 30
                width:rect.width-30
                height: 20
                anchors.horizontalCenter: parent.horizontalCenter
                color: "#454545"
                font.pixelSize: 14
            }
        }
        Rectangle {
            id:rect1
            anchors.top: rect.bottom
            anchors.topMargin: 10
            anchors.left: parent.left
            anchors.leftMargin: 10
            anchors.right: parent.right
            anchors.rightMargin: 10
            radius: 9
            color: "#D39B59"
            anchors.horizontalCenter: parent.horizontalCenter
            height: 90

            Rectangle {
                id:rect2
                anchors.top: rect1.top
                anchors.topMargin: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.right: parent.right
                anchors.rightMargin: 10
                radius: 9
                color: "white"
                anchors.horizontalCenter: parent.horizontalCenter
                height: 30

                Text {
                    id:txttt
                    anchors.top: parent.top
                    anchors.topMargin: 5
                    anchors.leftMargin: 73
                    anchors.left: rect2.left
                    text: "Link a card"
                    color: "#383838"
                    font.pixelSize: 14

                }
                Image {
                    id: mir
                    anchors.top:parent.top
                    anchors.left:parent.left
                    anchors.topMargin: 7
                    anchors.leftMargin: 7

                    source: "qrc:/res/mir.png"
                    width: 50
                    height: parent.height / 2
                }
                Image {
                    id: plus
                    anchors.top:parent.top
                    anchors.right:parent.right
                    anchors.topMargin: 7
                    anchors.rightMargin: 7

                    source: "qrc:/res/plus.png"
                    width: 15
                    height: parent.height / 2
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            console.log("add")
                        }
                    }
                }



            }

            Rectangle {
                id:rect3
                anchors.top: rect2.bottom
                anchors.topMargin: 10
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.right: parent.right
                anchors.rightMargin: 10
                radius: 9
                color: "white"
                anchors.horizontalCenter: parent.horizontalCenter
                height: 30
                Text {
                    id:txt0
                    anchors.top: parent.top
                    anchors.topMargin: 5
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.left: rect.left
                    text: "Link bank with SBP"
                    color: "#383838"
                    font.pixelSize: 14

                }

                Image {
                    id: spb
                    anchors.top:parent.top
                    anchors.left:parent.left
                    anchors.topMargin: 7
                    anchors.leftMargin: 15

                    source: "qrc:/res/spb.png"
                    width: 30
                    height: parent.height/2
                }
                Image {
                    id: plus1
                    anchors.top:parent.top
                    anchors.right:parent.right
                    anchors.topMargin: 7
                    anchors.rightMargin: 7

                    source: "qrc:/res/plus.png"
                    width: 15
                    height: parent.height / 2
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            console.log("add")
                        }
                    }
                }
            }

        }


    }






    FooterPay{
        id: foot
        anchors.bottom: parent.bottom
        width: parent.width // Присвойте ширину contentArea

        Text {
            id: ttt
            color: "white"
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            font.pixelSize: 14
            text: "To pay"
            font.bold: true
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    console.log("PAY")
                }
            }
        }
    }



}


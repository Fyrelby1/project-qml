import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

Rectangle {
    width: parent.width
    height: 50
    color: "#D39B59"

    property var boximg
    property var homeimg
    property var porfolioimg

    signal boxClicked
    signal homeClicked
    signal profileClicked

    Row {
        spacing: 10

        // Кнопка "Корзина"
        Rectangle {
            id: box
            anchors.top: parent.top
            anchors.topMargin: 12
            anchors.left: parent.left
            anchors.leftMargin: 65
            width: 20
            height: 20
            color: "transparent"

            Image {
                anchors.fill: parent
                source: boximg // Замените путь на свой путь к изображению бургера
            }

            MouseArea {
                anchors.fill: parent
                onClicked: boxClicked() // испускается сигнал boxClicked при клике
            }
        }

        // Кнопка "Главная"
        Rectangle {
            id: home
            width: 20
            height: 20
            color: "transparent"
            anchors.top: parent.top
            anchors.topMargin: 12
            anchors.left: box.left
            anchors.leftMargin: 80

            Image {
                anchors.fill: parent
                source: homeimg // Замените путь на свой путь к изображению бургера
            }

            MouseArea {
                anchors.fill: parent
                onClicked: homeClicked() // испускается сигнал homeClicked при клике
            }
        }

        // Кнопка "Профиль"
        Rectangle {
            id: prof
            width: 20
            height: 20
            color: "transparent"
            anchors.top: parent.top
            anchors.topMargin: 12
            anchors.left: home.left
            anchors.leftMargin: 80

            Image {
                anchors.fill: parent
                source: porfolioimg // Замените путь на свой путь к изображению бургера
            }

            MouseArea {
                anchors.fill: parent
                onClicked: profileClicked() // испускается сигнал profileClicked при клике
            }
        }
    }

}

import QtQuick 2.15
import QtQuick.Controls 2.15

Rectangle {
    width: 30
    height: 17
    color: "transparent"

    Image {
        anchors.fill: parent
        source: "res/burg.png" // Замените путь на свой путь к изображению бургера
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            main_page.hide()
            page_burger.show()
        }
    }
}

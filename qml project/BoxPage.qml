import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5



Window{
    id: win
    width: 320
    height: 650
    visible: true
    color: "#F4E6D6"
    title: "Корзина"
    function updateSum() {
        var totalCost = 43;
        for (var i = 0; i < my_model.count; ++i) {
            totalCost += my_model.get(i).score * parseInt(my_model.get(i).cost.replace("$", ""));
        }
        textToUpdate.text = "Total Cost: $" + totalCost;
    }
    Page {
        HeaderWithBack{anchors.topMargin: 10}
        id: page
        anchors.fill: parent
        background: null
        Rectangle{
            id: bg_list
            color: "white"
            anchors.fill: parent
            anchors.centerIn: parent
            anchors.topMargin: 55
            width: 300
            height: 600
            radius: 25
            Rectangle{
                color: "white"
                anchors.fill: parent
                anchors.centerIn: parent
                anchors.topMargin: 20
                anchors.bottomMargin: 50
                width: 300
                Component {
                    id: my_delegate
                    Orders {
                        width: parent.width * 0.95
                        height: 150
                        anchors.centerIn: win
                        onScoreChanged: {
                            my_list.model.setProperty(index, "score", currentScore);
                            updateSum();
                        }
                    }
                }

                ListView {
                    id: my_list
                    clip: true
                    anchors.fill: parent
                    model: my_model
                    delegate: my_delegate

                    spacing: 20
                }
                ListModel {
                    id: my_model
                    ListElement {
                        imagePath: "qrc:/res/toy1.png"
                        nametoy: "Toy Wooden Tools"
                        cost: "$23"
                    }
                    ListElement {
                        imagePath: "qrc:/res/toy2.jpg"
                        nametoy: "Toy Car"
                        cost: "$20"

                    }
                }

                Rectangle{
                    width: 280
                    height: 45
                    radius: 36
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 15
                    color: "#6E492A"
                    Text{
                        id: textToUpdate
                        anchors.centerIn: parent
                        color: "white"
                        text: "Total Cost: $43"
                    }

                    MouseArea{
                        anchors.fill: parent
                        onClicked: {
                            console.log("оформление покупки")
                            updateSum();
                        }
                    }
                }

            }

        }
        Footer{
            anchors.bottom: parent.bottom
            boximg: "qrc:/res/boxDark.png"
            homeimg: "qrc:/res/home.png"
            porfolioimg: "qrc:/res/porfolio.png"
            onHomeClicked: {

                    var mainPageComponent = Qt.createComponent("MainPage.qml");
                    if (mainPageComponent.status === Component.Ready) {
                        var mainPage = mainPageComponent.createObject(win);
                        mainPage.visible = true;
                        win.visible = false;

                    }
                }
            onProfileClicked: {
                var profilePageComponent = Qt.createComponent("ProfilePage.qml");
                if (profilePageComponent.status === Component.Ready) {
                    var profilePage = profilePageComponent.createObject(win);
                    win.visible = false;
                    profilePage.visible = true;
                }
            }
        }
    }
}



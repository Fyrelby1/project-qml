import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.53

Window {
    id: win_record
    visible: false
    width: 320
    height: 650
    title: "Запись сайта"

    Rectangle{
            anchors.fill:parent
            gradient:Gradient{
            GradientStop{position:0;color:"#F4E6D6"}
            GradientStop{position:1;color:"#F4E6D6"}
            }
        }

    Header{
        id: header
        anchors.top: parent.top
    }

    // Основное содержимое страницы
    MouseArea {
        height: 30
        width: header.width / 3
        anchors{
            top: header.bottom
            topMargin: 5
            right: header.right
            rightMargin: 10
        }
        Rectangle {
               radius: 30
               anchors.fill: parent
               border.width: 1
               border.color: "#F8C486"
               gradient: Gradient {
                   GradientStop { position: 0; color: "#6E492A" }
                   GradientStop { position: 1; color: "#6E492A" }
               }
        }
        Text{
            text: "View more"
            color: "#FFFFFF"
            anchors.centerIn: parent
            font.pixelSize: 10
        }
        onClicked: {
            win_record.close()
            master_list.show()
        }
        MasterPage{id:master_list}

    }

    Image{
        id: calendar
        source: "res/calendar.png"
        anchors.top: header.bottom
        anchors.topMargin: 40
        width: parent.width
        height: parent.height / 2.5
    }

    Rectangle {
        width: parent.width - 10
        height: 220
        color: "#FFFFFF"
        radius: 11
        anchors{
            top: calendar.bottom
            topMargin: 5
            left: parent.left
            leftMargin: 5
            right: parent.right
            rightMargin: 5
        }

        Image {
            id: leftImage
            source: "res/chel.png"
            anchors{
                top: parent.top
                topMargin: 5
                left: parent.left
                leftMargin: 5
                verticalCenter: parent.verticalCenter
            }
            height: 150
            width: 120
        }

        Text {
            id: textt
            text: "Welcome to an exciting journey into the world of creating toys with your own hands! Our master class is a unique opportunity to get acquainted with the techniques of making toys of various types, from funny soft animals to original educational games." // Текст, который вы хотите отобразить
            font.pixelSize: 10
            anchors{
                top: parent.top
                topMargin: 5
                left: leftImage.right
                leftMargin: 10
                right: parent.right
                rightMargin: 5
            }
            wrapMode: Text.WordWrap
        }
        Text{
            id:time
            text: "17:00"
            font.pixelSize: 16
            anchors{
                top: textt.bottom
                topMargin: 30
                left: textt.left
            }
        }

        MouseArea {
            height: 40
            anchors{
                top: textt.bottom
                topMargin: 72
                left: leftImage.right
                leftMargin: 8
                right: parent.right
                rightMargin: 8
            }
            Rectangle {
                   radius: 30
                   anchors.fill: parent
                   border.width: 1
                   border.color: "#F8C486"
                   gradient: Gradient {
                       GradientStop { position: 0; color: "#6E492A" }
                       GradientStop { position: 1; color: "#6E492A" }
                   }
            }
            Text{
                text: "Sign up for a master-class"
                color: "#FFFFFF"
                anchors.centerIn: parent
            }
            onClicked: {
                console.log("Sing up")
                win_record.close()
                page_pay.show()
            }

        }
   }


    Footer {
        id: footer
        anchors.bottom: parent.bottom
        boximg: "res/box.png"
        homeimg: "res/home.png"
        porfolioimg: "res/porfolio.png"
        onHomeClicked: {
                var mainPageComponent = Qt.createComponent("MainPage.qml");
                if (mainPageComponent.status === Component.Ready) {
                    var mainPage = mainPageComponent.createObject(win_record);
                    mainPage.visible = true;
                    win_record.visible = false;

                }
            }
        onBoxClicked: {
            // создаем и открываем страницу корзины
            var boxPageComponent = Qt.createComponent("BoxPage.qml");
            if (boxPageComponent.status === Component.Ready) {
                var boxPage = boxPageComponent.createObject(win_record);
                win_record.visible = false;
                boxPage.visible = true;
            }
        }
        onProfileClicked: {
            // создаем и открываем страницу профиля
            var profilePageComponent = Qt.createComponent("ProfilePage.qml");
            if (profilePageComponent.status === Component.Ready) {
                var profilePage = profilePageComponent.createObject(win_record);
                win_record.visible = false;
                profilePage.visible = true;
            }
        }
    }
    PagePay{id: page_pay}
}

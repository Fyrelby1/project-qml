import QtQuick 2.15
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5

Item {
    id: deleg
    Rectangle{
        id:rect
        radius:20
        anchors.fill: parent
        anchors.leftMargin: 15
        anchors.topMargin: 10
        color: "white"
        GridLayout{
            columns: 2
            rows: 1
            columnSpacing: 10
            anchors.centerIn: parent
            Rectangle{
                width: 120
                height: 120
                Image {
                    width: 120
                    height: 120
                    source: imagePath
                }
            }
            Text {
                id: name
                text: infomaster
                font.pixelSize: 8
            }
        }
    }

    Button{
        id: control
        width: 40
        height: 40
        anchors.right: parent.right
        anchors.top: rect.bottom
        anchors.topMargin: -15
        anchors.rightMargin: 15
        background: Rectangle {
            width: 40
            height: 40
            color: "#6E492A"
            radius: 40
            Image {
                source: "qrc:/res/trian.png"
            }
        }
        onClicked: {
            prof_masters.show()
            win_master_list.hide()
        }
    }
}

import QtQuick 2.15

Rectangle {
    id: mast
    anchors.top: header.bottom
    anchors.bottom: footer.top
    anchors.topMargin: 5
    anchors.bottomMargin: 5
    radius: 30
    width: parent.width
    height: parent.height
    property var text1

    Text {
        id: naming
        text: text1
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 20 // Расстояние от верхнего края прямоугольника до текста
    }

    Rectangle {
        width: parent.width - 40 // Ширина линии (прямоугольника) с учетом отступов по 5 с обеих сторон
        height: 1 // Высота линии
        color: "#454545" // Цвет линии
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: naming.bottom // Размещаем линию под текстом
        anchors.topMargin: 5 // Отступ между текстом и линией
    }
    Text {
        id: centerText
        text: "The list is empty"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter // Размещаем текст по центру прямоугольника
    }
}

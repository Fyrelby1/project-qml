import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

Rectangle {
    width: parent.width
    height: 50
    color: "transparent"

    Row {
        spacing: 10

        // Бургер
        Burger{
            id: bur
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.leftMargin: 10
            anchors.topMargin: 20
        }

        // Название сайта
        Image {
            id: name
            source: "res/name.png" // Замените путь на свой путь к изображению логотипа сайта
            width: 100
            height: 30
            anchors.top: parent.top
            anchors.topMargin: 10
            anchors.left: bur.right
            anchors.leftMargin: 70
            horizontalAlignment: Qt.AlignHCenter
        }

        // Уведомления
        Uvedoml{
            id: u
            anchors.left: name.right
            anchors.top: parent.top
            anchors.leftMargin: 75
            anchors.topMargin: 20
        }
    }
}

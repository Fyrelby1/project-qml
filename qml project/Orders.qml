import QtQuick 2.15
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5

Item {
    id: deleg
    property int currentScore: 1
    signal scoreChanged(int newScore)
    function updateScore(value) {
        currentScore += value
        if (currentScore < 1){
            currentScore = 1
        }
        scoreChanged(currentScore)
    }
    Rectangle{
        id: rect
        radius:20
        anchors.fill: parent
        anchors.leftMargin: 15
        anchors.topMargin: 10
        color: "white"
        GridLayout{
            columns: 2
            rows: 1
            columnSpacing: 15
            Rectangle{
                width: 120
                height: 120
                Image {
                    width: 120
                    height: 120
                    source: imagePath
                }
            }
            ColumnLayout{
                Layout.alignment: Qt.AlignTop
                Text {
                    id: name
                    text: nametoy
                    font.pixelSize: 12
                }
                RowLayout{
                    spacing: 25
                    Text {
                        id: pay
                        text: cost
                        font.pixelSize: 20
                    }
                    Rectangle{
                        radius: 66
                        width: 90
                        height: 25
                        color: "#DFDFDF"

                        Button{
                            width: 20
                            height: 20
                            id: minus
                            text: "-"
                            anchors.left: parent.left
                            anchors.verticalCenter: parent.verticalCenter
                            Rectangle{
                                width: 20
                                height: 20
                                radius: 20
                                color: "#DFDFDF"
                            }
                            anchors.leftMargin: 5
                            onClicked: updateScore(-1)
                        }
                        Text{
                            id: score
                            text: currentScore.toString()
                            anchors.centerIn: parent
                        }

                        Button{
                            width: 20
                            height: 20
                            id: plus
                            text: "+"
                            anchors.right: parent.right
                            anchors.verticalCenter: parent.verticalCenter
                            Rectangle{
                                width: 20
                                height: 20
                                radius: 20
                                color: "#DFDFDF"
                            }
                            anchors.rightMargin: 5
                            onClicked: updateScore(1)
                        }
                    }
                }
            }
        }
    }
}

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.3


ApplicationWindow {
    id: main_page
    visible: false
    width: 320
    height: 650
    title: "Главная страница сайта"

    Rectangle{
        anchors.fill:parent
        gradient:Gradient{
            GradientStop{position:0;color:"#F4E6D6"}
            GradientStop{position:1;color:"#F4E6D6"}
        }
    }

    Header {
        id: header
        anchors.top: parent.top
    }

    Rectangle {
        id: mast
        anchors.top: header.bottom
        anchors.topMargin: 5
        radius: 10
        width: 300
        anchors.horizontalCenter: parent.horizontalCenter
        height: parent.height / 3
        Rectangle{
            id: mask
            width: 280
            height: 150
            radius: 30
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 15
            Image {
                width: 280
                height: 150
                source: "res/masterMainPage.png"
                anchors.fill: mask
            }

        }
        Button {
            Text {
                color: "white"
                text: "Attend a master class"
                anchors.centerIn: parent
            }

            width: mask.width / 1.2
            height: 35
            anchors.left: mask.left
            anchors.leftMargin: 25
            anchors.bottom: mast.bottom
            anchors.bottomMargin: 10
            background: Rectangle{
                radius: 12
                color: "#D39B59"
            }
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    main_page.hide()
                    record.show()
                }
            }
            PageRecord{id: record}

        }
    }

    Rectangle{
        id: ll
        width: 300
        height: parent.height
        radius: 11
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: mast.bottom
        anchors.topMargin: 20
        color: "white"
        Component {
            id: my_delegate
            List_products {
                width: parent.width * 0.9
                height: 1500
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: parent.top
                anchors.topMargin: 25

            }
        }

        ListView {
            id: my_list
            clip: true
            anchors.fill: parent
            model: my_model
            delegate: my_delegate
            spacing: 10
            boundsBehavior: Flickable.DragOverBounds
        }
        ListModel {
            id: my_model
            ListElement {
                imagePath: "res/toy2.jpg"
                name: "Toy car"
                cost: "$20"
                imagePath2: "res/toy3.jpg"
                name2: "Toy train"
                cost2: "$65"
                imagePath3: "res/toy4.jpg"
                name3: "Toy tractor"
                cost3: "$70"
                imagePath4: "res/toy1.png"
                name4: "Toy Tools"
                cost4: "$23"
            }
        }
    }
    Footer {
        anchors.bottom: parent.bottom
        boximg: "res/box.png"
        homeimg: "res/homeDark.png"
        porfolioimg: "res/porfolio.png"
        onBoxClicked: {
            // создаем и открываем страницу корзины
            var boxPageComponent = Qt.createComponent("BoxPage.qml");
            if (boxPageComponent.status === Component.Ready) {
                var boxPage = boxPageComponent.createObject(main_page);
                main_page.visible = false;
                boxPage.visible = true;
            }
        }
        onProfileClicked: {
            // создаем и открываем страницу профиля
            var profilePageComponent = Qt.createComponent("ProfilePage.qml");
            if (profilePageComponent.status === Component.Ready) {
                var profilePage = profilePageComponent.createObject(main_page);
                main_page.visible = false;
                profilePage.visible = true;
            }
        }
    }
    PageUvedomlenia{id: page_uvedom}
    BurgerPage{id: page_burger}
}

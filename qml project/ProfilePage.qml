import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.53

Window {
    id:win_profile
    visible: false
    width: 320
    height: 650
    title: "Главная страница сайта"

    Rectangle{
        anchors.fill:parent
        gradient:Gradient{
            GradientStop{position:0;color:"#F4E6D6"}
            GradientStop{position:1;color:"#F4E6D6"}
        }
    }

    HeaderforProfile {
        id: header
        anchors.top: parent.top
        brimg: "res/back.png"
    }
    ColumnLayout {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: header.top
        anchors.topMargin: 60
        spacing: 10

        Rectangle {
            id: rectangle1
            width: 300
            height: 80
            radius: 30
            color: "#FFFFFF"

            Rectangle{
                id:ava
                anchors.bottom: rectangle1.top
                anchors.right: rectangle1.right
                anchors.rightMargin: 80
                anchors.bottomMargin: 15
                Image {
                    id: me
                    source: "res/ava.png"
                    width:50
                    height: 50
                }
            }

            Text{
                anchors.bottom: rectangle1.top
                anchors.right: ava.left
                anchors.rightMargin: 5
                text: "<b>Elizabeth Willis</b>"
            }

            RowLayout {
                anchors.left: rectangle1.left
                anchors.leftMargin: 40
                anchors.top: rectangle1.top
                anchors.topMargin: 20
                //anchors.fill: rectangle1
                spacing: 70
                Rectangle{
                    Image {
                        id: like
                        source: "res/like.png"  // Replace with your image source
                        width: 30
                        height: 30

                        Text {
                            text: "Favourites"
                            font.pixelSize: 10
                            anchors.top: like.bottom
                            anchors.horizontalCenter: like.horizontalCenter
                            MouseArea{
                                anchors.fill: parent
                                onClicked: {
                                    console.log("click")
                                    win_profile.close()
                                    page_fav.show()
                                }
                            }
                        }
                    }

                }

                Rectangle{
                    Image {
                        id: order
                        source: "res/oreder.png"  // Replace with your image source
                        width: 30
                        height: 30

                        Text {
                            text: "Orders"
                            font.pixelSize: 10
                            anchors.top: order.bottom
                            anchors.horizontalCenter: order.horizontalCenter
                        }
                    }
                }

            }
        }

        Rectangle {
            id: rectangle2
            width: 300
            height: 200
            radius: 30
            color: "#FFFFFF"

            Rectangle{
                id: under
                anchors.top: rectangle2.top
                anchors.left: rectangle2.left
                anchors.leftMargin: 30
                anchors.topMargin: 10
                width:30
                height: 30
                Image {
                    id: orders
                    source: "res/orders.png"  // Replace with your image source
                    width: 30
                    height: 30

                    Text {
                        text: "Orders"
                        font.pixelSize: 10
                        anchors.top: orders.top
                        anchors.left: orders.right
                        anchors.topMargin: 10
                    }
                }
            }

            Rectangle {
                id: line
                width: rectangle2.width - 40 // Ширина линии (прямоугольника) с учетом отступов по 5 с обеих сторон
                height: 1 // Высота линии
                color: "#D9D9D9" // Цвет линии
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: under.bottom // Размещаем линию под текстом
                anchors.topMargin: 5 // Отступ между текстом и линией
            }
            Text {
                id:pay
                text: "Payment is expected"
                font.pixelSize: 12
                anchors.top: line.top
                anchors.left: line.left
                anchors.topMargin: 20
            }
            Text {
                id:ship
                text: "Shipment is expected"
                font.pixelSize: 12
                anchors.top: pay.bottom
                anchors.left: pay.left
                anchors.topMargin: 15
            }
            Text {
                id:shipped
                text: "Shipped"
                font.pixelSize: 12
                anchors.top: ship.bottom
                anchors.left: ship.left
                anchors.topMargin: 15
            }
            Text {
                id:rew
                text: "A review is expected"
                font.pixelSize: 12
                anchors.top: shipped.bottom
                anchors.left: shipped.left
                anchors.topMargin: 15
            }
        }

        Rectangle {
            id: rectangle3
            width: 300
            height: 140
            radius: 30
            color: "#FFFFFF"

            Rectangle{
                id: hhh
                anchors.top: rectangle3.top
                anchors.left: rectangle3.left
                anchors.leftMargin: 20
                anchors.topMargin: 20
                width:30
                height: 30
                Image {
                    id: set
                    source: "res/settings.png"  // Replace with your image source
                    width: 20
                    height: 20

                    Text {
                        text: "Settings"
                        font.pixelSize: 12
                        anchors.top: set.top
                        anchors.left: set.right
                        anchors.topMargin: 2
                        anchors.leftMargin: 10
                    }
                }
            }
            Rectangle{
                id: ll
                anchors.top: rectangle3.top
                anchors.left: rectangle3.left
                anchors.leftMargin: 20
                anchors.topMargin: 60
                width:30
                height: 30
                Image {
                    id: feed
                    source: "res/feddback.png"  // Replace with your image source
                    width: 20
                    height: 20

                    Text {
                        text: "Feedback"
                        font.pixelSize: 12
                        anchors.top: feed.top
                        anchors.left: feed.right
                        anchors.topMargin: 2
                        anchors.leftMargin: 10
                    }
                }
            }

            Rectangle{
                id: nn
                anchors.top: rectangle3.top
                anchors.left: rectangle3.left
                anchors.leftMargin: 20
                anchors.topMargin: 100
                width:30
                height: 30
                Image {
                    id: exit
                    source: "res/feddback.png"  // Replace with your image source
                    width: 20
                    height: 20

                    Text {
                        text: "Feedback"
                        font.pixelSize: 12
                        anchors.top: exit.top
                        anchors.left: exit.right
                        anchors.topMargin: 2
                        anchors.leftMargin: 10
                    }
                }
            }

        }
    }

    Footer {
        id: footer
        anchors.bottom: parent.bottom
        boximg: "res/box.png"
        homeimg: "res/home.png"
        porfolioimg: "res/profilio_touch.png"
        onHomeClicked: {
            var mainPageComponent = Qt.createComponent("MainPage.qml");
            if (mainPageComponent.status === Component.Ready) {
                var mainPage = mainPageComponent.createObject(win_profile);
                mainPage.visible = true;
                win_profile.visible = false;

            }
        }
        onBoxClicked: {
            var boxPageComponent = Qt.createComponent("BoxPage.qml");
            if (boxPageComponent.status === Component.Ready) {
                var boxPage = boxPageComponent.createObject(win_profile);
                win_profile.visible = false;
                boxPage.visible = true;
            }
        }
    }
    PageFavoirites{id: page_fav}
}
